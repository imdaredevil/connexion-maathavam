var express = require('express');
var session = require('express-session');
var swig = require('swig');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var helmet = require('helmet');
var request = require('request');
var DomParser = require('dom-parser');
var parser = new DomParser();
var multer = require('multer');
var grid = require("gridfs-stream");
var fs = require('fs');
var url = "mongodb://cibi:cibicool16@ds259210.mlab.com:59210/wordpic";
var app = express();


app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(session({secret: 'hattori',name: 'diegocookie', secure: true,
    httpOnly: true,resave: true,
    saveUninitialized: true}));
app.use(bodyParser.json());
app.use(helmet());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/views"));

const port = process.env.PORT || 3000;
const urltime = "http://api.timezonedb.com/v2/get-time-zone?key=SB9PQZN2N90S&by=zone&zone=Asia/Kolkata";
const upload = multer({dest : 'upload/'});

app.get("/",function(req,res){
	var sess = req.session;
	if(sess.rollno)
		res.redirect("/play");
	else
		res.render("siginn.html");
});

app.get("/logout",function(req,res){
	var sess = req.session;
	sess.rollno = undefined;
	res.redirect("/");
});
app.get("/activate",function(req,res){
	var qid=1;
	MongoClient.connect(url,function(err,db){
		if(err || (!(qid)))
			{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var sess = req.session;	
		if(!(sess.rollno && sess.rollno == "2016103515"))
		{
			res.redirect("/");return;
		}
		else
		{
		var dbo = db.db('wordpic');
		var users = dbo.collection("users-math");
		users.updateMany({},{$set : {"activation": qid}},function(){
				if(err)
					{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
				else
					res.render("message.html",{"message":"activated"});
		});
	}
} 	});
});

app.post('/signin',function(req,res){

	MongoClient.connect(url,function(err,db){
		if(err)
			{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var rollno = req.body.rollno;	
		if(!(rollno))
		{
			res.redirect("/");return;
		}
		else
		{
		rollno = rollno + "";
		var dbo = db.db('wordpic');
		var users = dbo.collection("users-math");
		users.find({"rollno":rollno}).toArray(function(err,result){
			if(err)
				{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
			else
			{
			if(result.length == 0)
			{
				res.render("signup.html",{"message" : "முதல்  முறை மட்டும்","rollno":rollno});
			}
			else
			{
			var sess = req.session;
				console.log("cibi");
				sess.rollno = rollno + "";
				res.redirect("/play");
			}

}	});

}  } });
});

app.post("/signup",function(req,res){
	MongoClient.connect(url,function(err,db){
		if(err)
			{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var rollno = req.body.rollno;	
		var name = req.body.name;
		var phone = req.body.phone;
		if(!(rollno && name && phone))
		{
			res.render("signup.html",{"message" : "முதல்  முறை மட்டும்"});
		}
		else
		{
		rollno = rollno + "";
		var dbo = db.db('wordpic');
		var users = dbo.collection("users-math");
		users.find({"rollno":rollno}).toArray(function(err,result){
			if(err)
				{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
			else
			{
			if(result.length == 0)
			{
				users.insertOne({"rollno":rollno,"name":name,"phone":phone});
				var sess = req.session;
				console.log("cibi");
				sess.rollno = rollno + "";
				res.redirect("/play");
			}
			else
			{
			var sess = req.session;
				console.log("cibi");
				sess.rollno = rollno + "";
				res.redirect("/play");
			}

	}	});

}	}	});
});

app.get('/addques',function(req,res){
	if(req.session.rollno &&  req.session.rollno == "2016103515")
		res.render("addques.html");
	else
		res.render("message.html",{message:"not authenticated"});
});


app.post('/putstatus',function(req,res){
	var status = req.body.status;
	var rollno = req.session.rollno;
	var qid = req.body.qid;
	if(status && rollno == "2016103515")
	{
		MongoClient.connect(url,function(err,db){
		if(err)
			{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('wordpic');
		var questions = dbo.collection("questions-math");
		questions.updateOne({"qid":qid},{$set : {"status": status}},function(){
				if(err)
					{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
				else if (status == 2)
					res.redirect("/activate?qid="+qid);
				else 
					res.render("message.html",{"message":"status changed for question" + qid});
		});
} 	});
	}
	else
		res.redirect("/");
});


app.post('/addques',upload.single('pic'),function(req,res){
  		var answer = req.body.answer;
  		var file = req.file;
  		if(!(answer && file))
  		{
  			res.redirect("/");return;
  		}
  		else
  		{
	MongoClient.connect(url,function(err,db){
		if(err)
			 {console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('wordpic');
		var ques = dbo.collection("questions-math");
		ques.find().toArray(function(err,result){
		var gr = grid(dbo,require("mongodb"));
		var writestream = gr.createWriteStream({filename : "imagei"});
		fs.createReadStream(file.path).pipe(writestream);
		writestream.on('close', function (fil) {
  	var qid = req.body.qid;
  	ques.insertOne({"qid":qid,"file":fil._id,"answer":answer,"status":1});
  	  	fs.unlink(file.path,function(){
		});
  	res.render("message.html",{"message":"successfully added","link":"i"});
		
  	});
});

}  });

}	});

app.get('/img',function(req,res){
	if(!(req.query.qid && req.session.rollno))
	{
		res.redirect("/");
		return;
	}
	var qid = req.query.qid;
	MongoClient.connect(url,function(err,db){
		if(err)
			{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('wordpic');
		var question = dbo.collection("questions-math");
		question.find({"qid":qid}).toArray(function(err,result){
			if(result.length == 0)
				{
				  res.send("invalid");
				  return;
				}
			var iid = result[0].file;
			console.log(result[0].qid);
			var gr = grid(dbo,require("mongodb"));
			var readstream = gr.createReadStream({_id : iid});
			readstream.pipe(res);
			readstream.on("close",function(){
				res.send();return;
			});
		});
	}	});
});

app.get("/l1",function(req,res){
	MongoClient.connect(url,function(err,db){
		var qid = req.query.qid;
		var qa = "q" + qid + "a";
		var qt = "q" + qid + "time";
		if(err)
			throw(err);
		var dbo = db.db('wordpic');
		var users = dbo.collection("users-math");
		users.find().toArray(function(err,result){
							var count = result.length;
							var count2 = 0;
							var newres = new Array();
							for(var i=0;i<result.length;i++)
							{
								var temp = {};
								temp["rollno"] = result[i].rollno;
								temp["name"] = result[i].name;
								temp["phone"] = result[i].phone;
								temp["ans"] = result[i][qa];
								if(result[i][qt])
								{
									var d = new Date(1000*parseInt(result[i][qt]));
								count2++;
								console.log(d);
								temp.hour = d.getUTCHours();
								temp.min = d.getUTCMinutes();
								temp.sec = d.getUTCSeconds();
								temp[qt] = result[i][qt];
								newres.push(temp);
							}

							}
				  			newres = newres.sort(function (a,b) {
				  					var x = a[qt];
				  					var y = b[qt];
				  					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				  			});
			res.render("leaderboard1.html",{"link":"yes","users": newres,"count2":count,"count1":count2});return;
		});
	});
});

app.get("/ques",function(req,res){
		var sess = req.session;
		var ques = req.query.qid;
		if(!(sess.rollno && ques) )
			res.redirect("/");
		else
		{
		MongoClient.connect(url,function(err,db){
	if(err)
		{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
	else
	{
				var dbo = db.db('wordpic');
				var questions = dbo.collection("questions-math");
				var users = dbo.collection("users-math");
				questions.find({"qid":ques}).toArray(function(err,result){
						if(err)
						{
							console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});
						}
					else
					{
				if(result.length == 0)
					res.render("question.html",{"clue": "ஒரு தமிழ்ச் சொல்","link":"yes","question":{"status":1}});
				else
				{
					console.log(result[0]);
					if(result[0].status != 2)
					{
						res.render("question.html",{"clue": "ஒரு தமிழ்ச் சொல்","link":sess.rollno,"question":result[0]});
					}
					else
				{
				users.updateOne({"rollno":sess.rollno},{$set : {"activation": 0}},function(err,resu)
				{
					if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
					else
						res.render("question.html",{"clue": "ஒரு தமிழ்ச் சொல்","link":sess.rollno,"question":result[0]});
				});
			}
	}	}	});

		}	


   });

}	});




app.get("/play",function(req,res){
	MongoClient.connect(url,function(err,db){
	if(err)
		{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
	else
	{
				var dbo = db.db('wordpic');
				var questions = dbo.collection("questions-math");	
				var sess = req.session;
				var rollno = sess.rollno;
				questions.find().toArray(function(err,result){
				if(err)
					{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
				else if(rollno)
					res.render("levelgrid.html",{"link":"yes","questions":result});
				else
					res.redirect("/");
				});
				
}	});

});

app.get("/checkans",function(req,res){
	var ans =  req.query.ans;
	var sess = req.session;
	var qid = req.query.qid;
	//sess.rollno = "2016103515";
	var qa = "q" + qid + "a";
	var qt = "q" + qid + "time";
	if(!(sess.rollno && ans))
		res.redirect("/");
	else
	{
		var myquery = { "rollno" : sess.rollno};
request(urltime, function (err, response, body) {
  if(err)
  		{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
  else
  {
			var xmlDoc = parser.parseFromString(body,"text/xml");
			console.log(body);
			var xmlDoc = parser.parseFromString(xmlDoc.getElementsByTagName("result")[0].innerHTML);
  			var time = xmlDoc.getElementsByTagName("timestamp")[0].innerHTML; // Print the error if one occurred // Print the response status code if a response was received// Print the HTML for the Google homepage.
		var nv = {};
		nv[qa] = ans;
		nv[qt] = time;
		console.log(nv);
		var newvalues = { $set: nv };
		console.log(newvalues);
		console.log(time);
			MongoClient.connect(url,function(err,db){
				var dbo = db.db('wordpic');
				var users = dbo.collection("users-math");
				users.updateOne(myquery, newvalues, function(err, resu) {
						if(err)
							{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
						else
						res.render("message.html",{"message":"பங்கேற்றதுக்கு நன்றி","link":"yes"});
				});
 			});
	}	});
	}
});

app.get("*",function(req,res){
	res.render("message.html",{"message":"தவறு செய்து விட்டாய்"});
});


app.listen(port,function(error){
	console.log("The server is started" + port);
});